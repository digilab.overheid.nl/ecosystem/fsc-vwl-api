# Making changes to the api

To get started with the FSC VWL API, follow these steps:

1. Clone the repository:

```shell
git clone git@gitlab.com:digilab.overheid.nl/ecosystem/fsc-vwl-api.git
```

2. Install the required dependencies:

```shell
asdf plugin add golang
asdf plugin add golangci-lint
asdf plugin add oapi-codegen
asdf plugin add pre-commit
asdf install
pre-commit install
```

3. Generate the Go client/server code:

```shell
make all
```

4. To use the local version of the openapi definitions, add this to the `go.mod` file of your project:
```shell
replace "gitlab.com/digilab.overheid.nl/ecosystem/fsc-vwl-api" => "../<path-to-here>"
```
