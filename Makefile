.PHONY: lint
lint:
	@echo "Linting..."
	@golangci-lint run
	@echo "\tDone"

.PHONY: generate
generate:
	@echo "Generating..."
	@go generate
	@echo "\tDone"

.PHONY: tidy
tidy:
	@echo "Tidying..."
	@go mod tidy
	@echo "\tDone"

.PHONY: all
all: generate tidy lint
