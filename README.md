# FSC VWL API

This project contains an OpenAPI definition and generated Go client/server code for the FSC Verwerkinglogging plugin.

## Explore the API documentation:
Open your web browser and navigate to [https://editor.swagger.io/](https://editor.swagger.io/) and load the definitions from `spec/openapi.yaml`.

## Use the api
Include the library in you project
```sh
go get gitlab.com/digilab.overheid.nl/ecosystem/fsc-vwl-api
```
